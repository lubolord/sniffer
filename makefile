all: link
Sniffer.o: Sniffer.cpp
	g++ -c -o Sniffer.o Sniffer.cpp
build: Sniffer.o
link: build
	g++ -o Sniffer Sniffer.o -lpthread
clean:
	rm -f *.o Sniffer